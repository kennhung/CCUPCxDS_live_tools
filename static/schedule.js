
function formatTime(date) {
    return date.toLocaleTimeString(navigator.language, {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false
    });
}

function getCurrentSchedule(schedule) {
    const current = schedule.find((v) => {
        const start = new Date(v.start);
        const end = new Date(v.end);
        const current = Date.now();

        return start < current && current < end;
    });

    if (current) {
        const start = new Date(current.start);
        const end = new Date(current.end);

        return `${formatTime(start)} ~ ${formatTime(end)} ${current.title}`;
    } else {
        return "None";
    }
}