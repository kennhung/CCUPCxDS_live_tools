
function removeBGClass(index, className) {
    return (className.match(/(^|\s)bg-\S+/g) || []).join(' ');
}