const express = require('express');
const axios = require('axios').create({
    timeout: 4000,
});
const http = require('http');
const path = require('path');
const { Server } = require("socket.io");
const { PrismaClient } = require('@prisma/client');
const asyncHandler = require('express-async-handler');
var morgan = require('morgan');

const prisma = new PrismaClient();

const app = express();

app.use(morgan('tiny'));

const server = http.createServer(app);
const io = new Server(server);

function getSetting() {
    return prisma.settings.findFirst({
        where: {
            id: 1,
        },
        include: {
            CurrentContest: true,
            CurrentSchedule: true,
        }
    });
}

function getInfoBarText() {
    return prisma.infoBarText.findMany({
        include: {
            usingSettings: true,
        },
    });
}

async function getBalloon(contest) {
    const balloonURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/balloons`;

    const resp = await axios.get(balloonURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getContestTeam(contest, teamId) {
    const teamURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/teams/${teamId}`;

    const resp = await axios.get(teamURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getAllContestTeam(contest) {
    const teamURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/teams`;

    const resp = await axios.get(teamURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getContest(contest) {
    const groupURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}`;

    const resp = await axios.get(groupURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getContestState(contest) {
    const groupURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/state`;

    const resp = await axios.get(groupURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getContestStatus(contest) {
    const groupURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/status`;

    const resp = await axios.get(groupURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getSubmissions(contest) {
    const submissionsURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/submissions`;

    const resp = await axios.get(submissionsURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getJudgements(contest) {
    const judgementsURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/judgements`;

    const resp = await axios.get(judgementsURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getProblems(contest) {
    const problemsURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/problems`;

    const resp = await axios.get(problemsURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getContestGroup(contest, groupId) {
    const groupURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/groups/${groupId}`;

    const resp = await axios.get(groupURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

async function getScoreboard(contest) {
    const groupURL = `${contest.domServerURL}/api/v4/contests/${contest.domContestId}/scoreboard?public=true`;

    const resp = await axios.get(groupURL, {
        auth: {
            username: contest.domAPIUser,
            password: contest.domAPIPassword,
        }
    });

    return resp.data;
}

app.use('/static', express.static('static'));

app.get('/', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/index.html'));
});

app.get('/balloon', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/balloon_control.html'));
});

// setting
app.get('/api/setting', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    resp.status(200).send(setting);
}));

// team
async function getProcessedTeam(contest, id) {
    const team = await getContestTeam(contest, id);
    const scoreboard = await getScoreboard(contest);

    let teamBoard;
    if (scoreboard) {
        teamBoard = scoreboard.rows.find((v) => {
            return v.team_id === team.id;
        });
    }

    return {
        team,
        teamBoard
    };
}

app.get('/api/currentTeam', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest && setting.currentTeamId) {
        const team = await getProcessedTeam(setting.CurrentContest, setting.currentTeamId);
        resp.status(200).json(team);
    } else {
        resp.status(204);
    }
}));

app.get('/api/team/:id', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest && req.params.id) {
        const team = await getProcessedTeam(setting.CurrentContest, req.params.id);
        resp.status(200).json(team);
    } else {
        resp.status(204);
    }
}));

app.get('/api/team', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest) {
        const teams = await getAllContestTeam(setting.CurrentContest);
        resp.status(200).json(teams);
    } else {
        resp.status(204);
    }
}));

// contest
app.get('/api/contest', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest) {
        const [contest, state, status] = await Promise.all([
            getContest(setting.CurrentContest),
            getContestState(setting.CurrentContest),
            getContestStatus(setting.CurrentContest)
        ]);

        resp.status(200).json({
            contest,
            state,
            status,
        });
    } else {
        resp.status(204);
    }
}));

// scoreboard
app.get('/api/scoreboard', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest) {
        const board = await getScoreboard(setting.CurrentContest);

        resp.status(200).json(board);
    } else {
        resp.status(204);
    }
}));

// info scroll
app.get('/api/info_scroll', asyncHandler(async (req, resp, next) => {
    const infos = await getInfoBarText();

    resp.status(200).json(infos);
}));

// balloon
function sendBalloon(balloon) {
    io.emit('new_balloon', {
        team: balloon.team.replace(`t${balloon.teamid}: `, ""),
        prob: balloon.problem,
        color: balloon.color,
    });
}

let prevBalloon;
async function checkBalloon() {
    try {
        const setting = await getSetting();
        if (setting && setting.CurrentContest) {
            getBalloon(setting.CurrentContest).then((data) => {
                if (prevBalloon) {
                    for (let i = 0; i < data.length; i++) {
                        const balloon = data[i];

                        const prev = prevBalloon.find((v) => v.balloonid === balloon.balloonid);
                        if (prev && prev.done === false && balloon.done === true) {
                            sendBalloon(balloon);
                        }
                    }
                }

                prevBalloon = data;

                setTimeout(checkBalloon, 2000);
            }).catch((err) => {
                console.error(err.message);
                setTimeout(checkBalloon, 2000);
            });
        }
        else {
            setTimeout(checkBalloon, 2000);
        }
    } catch (err) {
        console.log("error", err);
    }
}

app.get('/api/balloons', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest) {

        const balloons = await getBalloon(setting.CurrentContest);

        resp.status(200).json(balloons);
    }
    else {
        resp.status(204);
    }
}));

// submissions
app.get('/api/submissions', asyncHandler(async (req, resp, next) => {
    const settings = await getSetting();

    if (settings && settings.CurrentContest) {
        const [submissions, judgements, contestState] = await Promise.all([
            getSubmissions(settings.CurrentContest),
            getJudgements(settings.CurrentContest),
            getContestState(settings.CurrentContest),
        ]);

        submissions.sort((a, b) => (new Date(b.time)) - (new Date(a.time)));

        const filtered = submissions.map((v) => {
            let j = judgements.filter((v2) => v2.submission_id === v.id && v2.valid);

            j.sort((a, b) => (new Date(b.start_time)) - (new Date(a.start_time)));

            if (contestState.frozen && (new Date(v.time) > new Date(contestState.frozen))) {
                j = j.map((v) => {
                    return {
                        ...v,
                        judgement_type_id: null,
                    };
                })
            }

            return {
                ...v,
                judgements: j,
            }
        });

        resp.status(200).json(filtered);
    } else {
        resp.status(204);
    }
}));

// problems
app.get('/api/problems', asyncHandler(async (req, resp, next) => {
    const setting = await getSetting();
    if (setting && setting.CurrentContest) {

        const problems = await getProblems(setting.CurrentContest);

        resp.status(200).json(problems);
    }
    else {
        resp.status(204);
    }
}));

// Socket io
io.on('connection', (socket) => {
    socket.on('change_team', async (data) => {
        await prisma.settings.update({
            where: {
                id: 1,
            },
            data: {
                currentTeamId: data === '-1' ? null : data
            },
        });

        io.emit('setting_update');
    });

    socket.on('add_info_scroll', async (data) => {
        await prisma.infoBarText.create({
            data: {
                content: data.content,
            },
        });

        io.emit('update_info_scroll');
    });

    socket.on('pick_info_scroll', async (data) => {
        await prisma.settings.update({
            where: {
                id: 1,
            },
            data: {
                infoBars: {
                    connect: {
                        id: data.id,
                    },
                }
            }
        });

        io.emit('update_info_scroll');
    });

    socket.on('unpick_info_scroll', async (data) => {
        await prisma.settings.update({
            where: {
                id: 1,
            },
            data: {
                infoBars: {
                    disconnect: {
                        id: data.id,
                    },
                },
            },
        });

        io.emit('update_info_scroll');
    });

    socket.on('delete_info_scroll', async (data) => {
        await prisma.infoBarText.delete({
            where: {
                id: data.id,
            }
        });

        io.emit('update_info_scroll');
    });

    socket.on('qmsg_send', (data) => {
        io.emit('qmsg', data);
    });

    socket.on('redo_balloon', (data) => {
        sendBalloon(data);
    });

    socket.on('toggle_time_source', async () => {
        const setting = await getSetting();

        await prisma.settings.update({
            where: {
                id: 1,
            },
            data: {
                useContestTime: !setting.useContestTime,
            },
        });

        io.emit('setting_update');
    });
});

// pages
app.get('/page/balloon', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/balloon.html'));
});

app.get('/page/current_schedule', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/current_schedule.html'));
});

app.get('/page/latest_submissions', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/latest_submissions.html'));
})

app.get('/page/team_card', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/team_card.html'));
})

app.get('/page/team_info', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/team_info.html'));
})

app.get('/page/remain_time', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/remain_time.html'));
})

app.get('/page/scoreboard', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/scoreboard.html'));
})

app.get('/page/bottom_bar', (req, resp) => {
    resp.sendFile(path.join(__dirname, '/pages/bottom_bar.html'));
})

server.listen(4000, async () => {
    if (!(await getSetting())) {
        await prisma.settings.create({
            data: {
                id: 1,
            }
        });
    }

    checkBalloon();

    console.log('listening on *:4000');
});
